package opgave9;

import java.util.concurrent.Semaphore;

public class Threadclass extends Thread {
	public int trådnavn;
	public int otherTrådnavn;
	public Common common;

	static Semaphore semaphore = new Semaphore(1);

	public Threadclass(int trådnavn, int otherTrådnavn, Common common) {
		this.trådnavn = trådnavn;
		this.common = common;
		this.otherTrådnavn = otherTrådnavn;
	}

	//Peterson
	public void run() {
		for (int j = 0; j < 100; j++) {
			try {
				semaphore.acquire();
				common.opdaterGlobal();
				semaphore.release();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			common.tagerRandomTid();
		}
	}
}
