package tråde;

public class Test {

	public static void main(String[] args) throws InterruptedException {
		lottoraek[] rækker = new lottoraek[10000000];
//		int[] rigtige = new int[8];
		lottoraek vindertal = new lottoraek();

		// generer rækkerne
		for (int i = 0; i < 10000000; i++) {
			lottoraek lottoraek = new lottoraek();
			rækker[i] = lottoraek;
		}

		ThreadClass t1 = new ThreadClass(rækker, vindertal, 0, 1000000);
		ThreadClass t2 = new ThreadClass(rækker, vindertal, 1000000, 2000000);
		ThreadClass t3 = new ThreadClass(rækker, vindertal, 2000000, 3000000);
		ThreadClass t4 = new ThreadClass(rækker, vindertal, 3000000, 4000000);
		ThreadClass t5 = new ThreadClass(rækker, vindertal, 4000000, 5000000);
		ThreadClass t6 = new ThreadClass(rækker, vindertal, 5000000, 6000000);
		ThreadClass t7 = new ThreadClass(rækker, vindertal, 6000000, 7000000);
		ThreadClass t8 = new ThreadClass(rækker, vindertal, 7000000, 8000000);
		ThreadClass t9 = new ThreadClass(rækker, vindertal, 8000000, 9000000);
		ThreadClass t10 = new ThreadClass(rækker, vindertal, 9000000, 10000000);

		/*
		 * long startTime = System.nanoTime();
		 * 
		 * //Tjekker alle rækkerne og tæller op på den rigtige for (int i = 0; i <
		 * rækker.length; i++) { int antalRigtige = rækker[i].antalrigtige(vindertal);
		 * rigtige[antalRigtige] = rigtige[antalRigtige] + 1; }
		 * 
		 * long stopTime = System.nanoTime(); long elapsedTime = stopTime - startTime;
		 * 
		 * System.out.println("Tid: " + elapsedTime / 1000000000.0 + "s");
		 * 
		 */
		long startTime = System.nanoTime();

		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
		t7.start();
		t8.start();
		t9.start();
		t10.start();

		t1.join();
		t2.join();
		t3.join();
		t4.join();
		t5.join();
		t6.join();
		t7.join();
		t8.join();
		t9.join();
		t10.join();
		long stopTime = System.nanoTime();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tid: " + elapsedTime / 1000000000.0);
		System.out.println("Med 4 tråde var jeg på ca 0,27 sek");
		System.out.println("Tidligere tid uden tråde var lige under 1 sek");

		// int alle = 0;
		// //Printer antallet af vindere
		// for (int i = 0; i < rigtige.length; i++) {
		// System.out.println("Antal vindere: " + rigtige[i]);
		// alle += rigtige[i];
		// }
		//
		// System.out.println("Alle rækker: " + alle);

		// System.out.println(Arrays.toString(rigtige));
	}

}
