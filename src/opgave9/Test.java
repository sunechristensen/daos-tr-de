package opgave9;

public class Test {
	public static void main(String[] args) throws InterruptedException {
		Common common = new Common();

		Threadclass t1 = new Threadclass(0, 1, common);
		Threadclass t2 = new Threadclass(1, 0, common);

		long startTime = System.nanoTime();

		t1.start();
		t2.start();

		t1.join();
		t2.join();

		long stopTime = System.nanoTime();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tid: " + elapsedTime / 1000000000.0);

		System.out.println("common: " + common.getGlobal());
	}
	/*
	 * - Initialization:
	 * § flag[0] = false; flag[1] = false;turn=0;
	 * 
	 * - Entercritical-code
	 * § flag[me] = true;
	 * § turn = other;
	 * § while (flag[other] && turn ==other);
	 * 
	 * - Exitcritical-code
	 * -flag[me] = false
	 */

}
