package opgave10;

public class Test {
	public static void main(String[] args) throws InterruptedException {
		Common common = new Common();

		/*
		 * Med synchronized er der bare ændret i metodehovedet på
		 * common.countGlobal()
		 */

		Threadclass t1 = new Threadclass(0, 1, common);
		Threadclass t2 = new Threadclass(1, 0, common);

		long startTime = System.nanoTime();

		t1.start();
		t2.start();

		t1.join();
		t2.join();

		long stopTime = System.nanoTime();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tid: " + elapsedTime / 1000000000.0);

		System.out.println("common: " + common.getGlobal());
	}

}
