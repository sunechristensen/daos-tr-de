package opgave10;

public class Common {
	public int global = 0;

	public void tagerRandomTid() {
		int j = 0;
		int i = 0;
		while (j < 100000000) {
			j++;
			while (i < 1000) {
				i++;
			}
		}
	}

	public int getGlobal() {
		return global;
	}

	public synchronized void opdaterGlobal() {
		int temp;
		temp = global;
		tagerRandomTid();
		global = temp + 1;
	}
}
