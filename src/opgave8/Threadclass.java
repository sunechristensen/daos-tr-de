package opgave8;

public class Threadclass extends Thread {
	public int trådnavn;
	public int otherTrådnavn;
	public Common common;

	public Threadclass(int trådnavn, int otherTrådnavn, Common common) {
		this.trådnavn = trådnavn;
		this.common = common;
		this.otherTrådnavn = otherTrådnavn;
	}

	//Peterson
	public void run() {
		for (int j = 0; j < 100; j++) {
			common.flag[trådnavn] = true;
			common.turn = otherTrådnavn;
			while (common.flag[otherTrådnavn] && common.turn == otherTrådnavn) {
			}
			common.opdaterGlobal();
			common.flag[trådnavn] = false;
			common.tagerRandomTid();
		}
	}
}
