package opgave10;

public class Threadclass extends Thread {
	public int trådnavn;
	public int otherTrådnavn;
	public Common common;

	public Threadclass(int trådnavn, int otherTrådnavn, Common common) {
		this.trådnavn = trådnavn;
		this.common = common;
		this.otherTrådnavn = otherTrådnavn;
	}

	//Peterson
	public void run() {
		for (int j = 0; j < 100; j++) {
			common.opdaterGlobal();
			common.tagerRandomTid();
		}
	}
}
