package opgave8;

public class Common {
	public int global = 0;
	volatile int turn = 0;

	public boolean[] flag = new boolean[2];

	public Common() {
		flag[0] = false;
		flag[1] = false;
	}

	public void tagerRandomTid() {
		int j = 0;
		int i = 0;
		while (j < 10000) {
			j++;
			while (i < 10000) {
				i++;
			}
		}
	}

	public int getGlobal() {
		return global;
	}

	public void opdaterGlobal() {
		int temp;
		temp = global;
		tagerRandomTid();
		global = temp + 1;
	}
}
