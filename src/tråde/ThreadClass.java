package tråde;

import java.util.Arrays;

public class ThreadClass extends Thread {
	private lottoraek[] rækker;
	private int[] antalRigtige = new int[8];
	private lottoraek vindertal;
	private int start;
	private int slut;

	public ThreadClass(lottoraek[] rækker, lottoraek vindertal, int start, int slut) {
		super();
		this.rækker = rækker;
		this.vindertal = vindertal;
		this.start = start;
		this.slut = slut;
	}

	public void run() {
		int sum = 0;
		for (int i = start; i < slut; i++) {
			int rigtige = rækker[i].antalrigtige(vindertal);
			antalRigtige[rigtige] = antalRigtige[rigtige] + 1;
		}

		for (Integer tal : antalRigtige) {
			sum += tal;
		}
		System.out.println("Alle :" + sum);
		System.out.println(Arrays.toString(antalRigtige) + "\n");
	}
}
